import 'package:http/http.dart' as http;
import 'package:testproject/models/catigory_model.dart';
import 'package:testproject/models/farmId_model.dart';
import 'package:testproject/models/fav_farm_model.dart';
import 'dart:convert';

class FarmRepositories {
  Future<List<FavFarmModel>> getFavFarms() async {
    var url = Uri.parse('http://213.239.205.250:8080/farms/favorite');
    http.Response response = await http.get(url);
    if (response.statusCode == 201)
      return favFarmModelFromJson(response.body);
    else
      return null;
  }

  Future<List<CatigoryModel>> getByCatigory(String location) async {
    var url = Uri.parse('http://213.239.205.250:8080/farms/Location/$location');
    http.Response response = await http.get(url);
    if (response.statusCode == 201)
      return catigoryModelFromJson(response.body);
    else
      return null;
  }

  Future<FarmIdModel> getById(int id) async {
    var url = Uri.parse('http://213.239.205.250:8080/farms/get/id/$id');
    http.Response response = await http.get(url);
    if (response.statusCode == 201)
      return farmIdModelFromJson(response.body);
    else
      return null;
  }

  Future<List<DateTime>> getFarmReservationsById(int id) async {
    var url = Uri.parse('http://213.239.205.250:8080/orders/dates/$id');
    http.Response response = await http.get(url);

    var listOfReservedDates =
        List<String>.from(json.decode(response.body).map((x) => x));

    var dateTimeList =
        List<DateTime>.from(listOfReservedDates.map((e) => DateTime.parse(e)));
    print(dateTimeList);
    print("$dateTimeList");

    if (response.statusCode == 200) {
      return dateTimeList;
    } else
      return null;
  }
}
