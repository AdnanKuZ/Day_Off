import 'package:flutter/material.dart';

class Settings {
  static const Color BackGround_Color = Color(0xFFF8F8F8);
  static const Color Button_Color = Color(0xFF6ED681);
  static const Color Form_Border_Color = Color(0xFFDFE4EB);
  static const Color Text_Color = Colors.black;
}
