import 'package:flutter/material.dart';
import 'package:testproject/View/Screens/DetailedViewPage.dart';
import 'package:testproject/common/settings.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:testproject/blocs/farm_bloc.dart';
import 'package:testproject/models/farmId_model.dart';
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

// var choices = [
//   'غالية',
//   'رخيصة',
//   'عائلية',
// ];

class _HomePageState extends State<HomePage> {
  String location = "بلودان";
  final FarmBloc _farmBloc = FarmBloc();

  @override
  void initState() {
    super.initState();
    _farmBloc.getFavFarm();
    _farmBloc.getCatigoryFarm(location);
  }

  // var dropDownValue = choices[0];
  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    return Scaffold(
      backgroundColor: Settings.BackGround_Color,
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60),
        child: AppBar(
            title: Text(
              'استكشف',
              style: TextStyle(color: Settings.Text_Color, fontSize: 29),
            ),
            centerTitle: true,
            elevation: 0,
            backgroundColor: Colors.transparent,
            actions: [
              InkWell(
                  onTap: () {},
                  child: Icon(
                    Icons.alarm,
                    color: Settings.Button_Color,
                    size: 30,
                  )),
            ],
            leading: InkWell(
              onTap: () {},
              child: Icon(
                Icons.dehaze,
                color: Settings.Button_Color,
                size: 30,
              ),
            )),
      ),
      body: SafeArea(
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            SizedBox(
              height: 10,
            ),
            buildSearchSection(),
            SizedBox(
              height: 20,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  FittedBox(
                    fit: BoxFit.contain,
                    child: InkWell(
                        splashColor: Settings.Button_Color,
                        onTap: () {},
                        child: Text(
                          "عرض الكل",
                          textDirection: TextDirection.rtl,
                          style: TextStyle(
                              color: Settings.Button_Color, fontSize: 20),
                        )),
                  ),
                  Spacer(),
                  FittedBox(
                    fit: BoxFit.contain,
                    child: Text(
                      "أفضل المزارع",
                      textDirection: TextDirection.rtl,
                      style:
                          TextStyle(color: Settings.Text_Color, fontSize: 25),
                    ),
                  ),
                ],
              ),
            ),
            StreamBuilder(
                stream: _farmBloc.favStream,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  return orientation == Orientation.portrait
                      ? Container(
                          height: MediaQuery.of(context).size.height * 0.5,
                          child: buildFavFarms(snapshot, 0.6, _farmBloc))
                      : Container(
                          height: MediaQuery.of(context).size.height * 0.8,
                          child: buildFavFarms(snapshot, 0.3, _farmBloc),
                        );
                }),
            SizedBox(
              height: 40,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 8),
              child: Row(
                children: [
                  Spacer(),
                  FittedBox(
                    fit: BoxFit.contain,
                    child: Text(
                      "أفضل مزارع الزبداني",
                      textDirection: TextDirection.rtl,
                      style:
                          TextStyle(color: Settings.Text_Color, fontSize: 25),
                    ),
                  ),
                ],
              ),
            ),
            StreamBuilder(
                stream: _farmBloc.catigoryStream,
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  return orientation == Orientation.portrait
                      ? Container(
                          height: MediaQuery.of(context).size.height * 0.5,
                          child: buildFavFarms(snapshot, 0.6, _farmBloc))
                      : Container(
                          height: MediaQuery.of(context).size.height * 0.8,
                          child: buildFavFarms(snapshot, 0.3, _farmBloc),
                        );
                }),
          ],
        ),
      ),
    );
  }

  Widget buildSearchSection() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8),
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            InkWell(
              splashColor: Settings.Button_Color,
              onTap: () {},
              child: Container(
                height: 40,
                width: 40,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, color: Settings.Button_Color),
                child: Icon(
                  Icons.search,
                  color: Colors.white,
                ),
              ),
            ),
            SizedBox(
              width: 10,
            ),
            FittedBox(
              fit: BoxFit.contain,
              child: Text(
                "إبحث عن مزرعة تناسبك",
                textDirection: TextDirection.rtl,
                style: TextStyle(
                    color: Settings.Text_Color,
                    fontSize: 26,
                    fontWeight: FontWeight.w600),
              ),
            ),
            // DropdownButton<String>(
            //   iconEnabledColor: Settings.Button_Color,
            //   value: dropDownValue,
            //   elevation: 10,
            //   style: TextStyle(fontSize: 25, color: Settings.Text_Color),
            //   icon: Icon(Icons.arrow_downward),
            //   iconSize: 25,
            //   items: choices.map((String dropDownStringItem) {
            //     return DropdownMenuItem<String>(
            //       child: Text(dropDownStringItem),
            //       value: dropDownStringItem,
            //     );
            //   }).toList(),
            //   onChanged: (String newValue) {
            //     setState(() {
            //       dropDownValue = newValue;
            //     });
            //   },
            // ),
          ],
        ),
      ),
    );
  }
}

Widget buildFavFarms(AsyncSnapshot snapshot, num width, FarmBloc farmBloc) {
  var snapshotData = snapshot.data;
  if (snapshot.hasData) {
    return Container(
      child: ListView.separated(
        physics: BouncingScrollPhysics(),
        reverse: true,
        scrollDirection: Axis.horizontal,
        padding: EdgeInsets.symmetric(horizontal: 13, vertical: 3),
        separatorBuilder: (context, index) => SizedBox(width: 13),
        itemCount: snapshotData.length,
        itemBuilder: (context, index) {
          IdBloc idBloc = Provider.of<IdBloc>(
            context,
            listen: false,
          );
          return Container(
              width: MediaQuery.of(context).size.width * width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(15)),
                color: Colors.white,
                boxShadow: [
                  BoxShadow(
                    color: Colors.grey,
                    blurRadius: 2.0,
                    spreadRadius: 0.0,
                    offset: Offset(2.0, 2.0),
                  )
                ],
              ),
              child: Column(children: [
                Expanded(
                  flex: 4,
                  child: InkWell(
                    onTap: () {
                      idBloc.setId = snapshotData[index].id;
                      Navigator.pushNamed(context, "detailed");
                    },
                    child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        child: CachedNetworkImage(
                          fit: BoxFit.cover,
                          imageUrl: snapshotData[index].photos[0],
                          placeholder: (context, url) =>
                              Center(child: CircularProgressIndicator()),
                        )),
                  ),
                ),
                Expanded(
                  flex: 1,
                  child: Padding(
                    padding: const EdgeInsets.only(
                        left: 2, right: 2, top: 2, bottom: 2),
                    child: FittedBox(
                      fit: BoxFit.contain,
                      alignment: Alignment.center,
                      child: Row(
                        textDirection: TextDirection.rtl,
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Text(
                            "${snapshotData[index].price}",
                            style: TextStyle(
                                color: Settings.Button_Color, fontSize: 21),
                          ),
                          Text(
                            "/ليلة",
                            textDirection: TextDirection.rtl,
                            style: TextStyle(
                                fontSize: 15, color: Settings.Text_Color),
                          ),
                          Icon(
                            Icons.location_pin,
                            color: Settings.Button_Color,
                          ),
                          Text(
                            "${snapshotData[index].location}",
                            textDirection: TextDirection.rtl,
                            style: TextStyle(
                                fontSize: 15, color: Settings.Text_Color),
                          ),
                        ],
                      ),
                    ),
                  ),
                )
              ]));
        },
      ),
    );
  } else if (snapshot.hasError) {
    return Center(
      child: Text(
        "يوجد خطأ بالإتصال",
        textDirection: TextDirection.rtl,
      ),
    );
  } else {
    return Center(
      child: Container(
        child: CircularProgressIndicator(),
      ),
    );
  }
}
