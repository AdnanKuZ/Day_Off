import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:testproject/View/widgets/booking_widget.dart';
import 'package:testproject/common/settings.dart';
import 'package:testproject/blocs/booking_bloc.dart';
import 'package:provider/provider.dart';
import 'package:testproject/blocs/farm_bloc.dart';

class BookingScreen extends StatelessWidget {
  final BookingBloc _bookingBloc = BookingBloc();
  @override
  Widget build(BuildContext context) {
    final _idBloc = Provider.of<IdBloc>(context, listen: false);
    _bookingBloc.addReservedDates(_idBloc.id);
    Orientation orientation = MediaQuery.of(context).orientation;
    bool portrait = orientation == Orientation.portrait;
    return Scaffold(
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        padding: portrait
            ? EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.001)
            : EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.04),
        decoration: BoxDecoration(
          color: Colors.grey.shade400,
        ),
        alignment: Alignment.center,
        child: Container(
          padding: portrait
              ? EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.08)
              : EdgeInsets.symmetric(
                  horizontal: MediaQuery.of(context).size.width * 0.1),
          width: MediaQuery.of(context).size.width,
          height: portrait
              ? MediaQuery.of(context).size.height * 0.55
              : MediaQuery.of(context).size.height * 0.75,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(25)),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                spreadRadius: 5,
                blurRadius: 7,
                offset: Offset(0, 3),
              )
            ],
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Expanded(flex: 1, child: Container()),
              Expanded(
                flex: 1,
                child: AutoSizeText(
                  "خيارات حجز المزرعة",
                  textDirection: TextDirection.rtl,
                  maxLines: 1,
                  maxFontSize: 32,
                  minFontSize: 11,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                    fontWeight: FontWeight.w200,
                  ),
                ),
              ),
              Expanded(
                  flex: portrait ? 2 : 1,
                  child: Container(
                    alignment: Alignment.center,
                    child: AutoSizeText(
                      "يرجى إختيار الأيام بالترتيب",
                      maxLines: 1,
                      maxFontSize: 17,
                      minFontSize: 8,
                      style: TextStyle(color: Colors.black54, fontSize: 15),
                    ),
                  )),
              Expanded(
                flex: 2,
                child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Expanded(
                          flex: 3,
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                AutoSizeText(
                                  "بدء الحجز",
                                  maxLines: 1,
                                  maxFontSize: 28,
                                  minFontSize: 10,
                                  style: TextStyle(
                                      color: Settings.Button_Color,
                                      fontSize: 19),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: StreamBuilder(
                                    stream: _bookingBloc.bookingDatesStream,
                                    builder: (context, snapshot) {
                                      List<DateTime> datesList = snapshot.data;
                                      if (snapshot.hasData) {
                                        return AutoSizeText(
                                          datesList[0]
                                              .toString()
                                              .replaceAll("-", "/")
                                              .substring(5, 10),
                                          maxLines: 1,
                                          maxFontSize: 30,
                                          minFontSize: 15,
                                          style: TextStyle(
                                              color: Colors.black87,
                                              fontSize: 25),
                                        );
                                      } else
                                        return Text('---',
                                            style: TextStyle(fontSize: 28));
                                    },
                                  ),
                                )
                              ],
                            ),
                          )),
                      Expanded(
                          flex: 1,
                          child: Center(
                            child: Icon(Icons.arrow_forward),
                          )),
                      Expanded(
                          flex: 3,
                          child: Container(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                AutoSizeText(
                                  "نهاية الحجز",
                                  maxLines: 1,
                                  maxFontSize: 28,
                                  minFontSize: 10,
                                  style: TextStyle(
                                      color: Settings.Button_Color,
                                      fontSize: 19),
                                ),
                                SizedBox(
                                  height: 8,
                                ),
                                Container(
                                  alignment: Alignment.center,
                                  child: StreamBuilder(
                                      stream: _bookingBloc.bookingDatesStream,
                                      builder: (context, snapshot) {
                                        print(snapshot.data.toString());
                                        List<DateTime> datesList =
                                            snapshot.data;
                                        if (snapshot.hasData) {
                                          return AutoSizeText(
                                            datesList[datesList.length - 1]
                                                .toString()
                                                .replaceAll("-", "/")
                                                .substring(5, 10),
                                            maxLines: 1,
                                            maxFontSize: 30,
                                            minFontSize: 15,
                                            style: TextStyle(
                                                color: Colors.black87,
                                                fontSize: 25),
                                          );
                                        } else
                                          return Text("---",
                                              style: TextStyle(fontSize: 28));
                                      }),
                                )
                              ],
                            ),
                          )),
                    ],
                  ),
                ),
              ),
              Expanded(
                  flex: portrait ? 2 : 1,
                  child: Container(
                    alignment: Alignment.center,
                    child: InkWell(
                      onTap: () {
                        showDialog(
                            context: context,
                            builder: (context) {
                              return StreamBuilder(
                                  stream: _bookingBloc.reservedDatesStream,
                                  builder: (context, snapshot) => snapshot
                                          .hasData
                                      ? BookingCalenderWidget(
                                          _idBloc.id, _bookingBloc, snapshot)
                                      : Center(
                                          child: CircularProgressIndicator()));
                            });
                      },
                      child: AutoSizeText(
                        "تاريخ جديد",
                        textDirection: TextDirection.rtl,
                        maxLines: 1,
                        maxFontSize: 28,
                        minFontSize: 12,
                        style: TextStyle(color: Colors.black, fontSize: 19),
                      ),
                    ),
                  )),
              Expanded(
                flex: 1,
                child: Container(
                  width: 300,
                  child: ElevatedButton(
                    onPressed: () {},
                    style: ButtonStyle(
                        backgroundColor:
                            MaterialStateProperty.resolveWith<Color>(
                                (Set<MaterialState> color) {
                          return Settings.Button_Color;
                        }),
                        shape:
                            MaterialStateProperty.all<RoundedRectangleBorder>(
                                RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ))),
                    child: Text(
                      "إحجز الآن",
                      textDirection: TextDirection.rtl,
                    ),
                  ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(),
              )
            ],
          ),
        ),
      ),
    );
  }
}
