import 'package:flutter/material.dart';
import 'package:testproject/common/settings.dart';

class SearchPage extends StatefulWidget {
  SearchPage({Key key}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        centerTitle: true,
        backgroundColor: Colors.transparent,
        leading: InkWell(
            onTap: () {},
            child: Icon(
              Icons.arrow_back,
              color: Settings.Button_Color,
            )),
        actions: [
          InkWell(
            onTap: () {},
            child: Icon(Icons.notifications, color: Settings.Button_Color),
          )
        ],
      ),
      // body: ,
    );
  }
}
