import 'dart:io';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:carousel_extended/carousel_extended.dart';
import 'package:testproject/blocs/farm_bloc.dart';
import 'package:testproject/View/widgets/booking_widget.dart';
import 'package:testproject/utils/app_icons.dart';
import 'package:testproject/common/settings.dart';
import 'package:provider/provider.dart';

class DetailedViewPage extends StatefulWidget {
  DetailedViewPage({Key key}) : super(key: key);

  @override
  _DetailedViewPage createState() => _DetailedViewPage();
}

class _DetailedViewPage extends State {
  final FarmBloc _farmBloc = FarmBloc();
  @override
  void initState() {
    final _idBloc = Provider.of<IdBloc>(context, listen: false);
    _farmBloc.getIdFarm(_idBloc.id);
    print("${_idBloc.id}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    Orientation orientation = MediaQuery.of(context).orientation;
    bool portrait = orientation == Orientation.portrait;
    return Scaffold(
        backgroundColor: Colors.white,
////////////////////////////////     AppBar
        appBar: PreferredSize(
          preferredSize: Size(MediaQuery.of(context).size.width, 40),
          child: AppBar(
            backgroundColor: Colors.transparent,
            elevation: 0,
            leading: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Icon(
                  Icons.arrow_back,
                  color: Settings.Button_Color,
                )),
          ),
        ),
////////////////////////////////     NavBar
        ///showDateRangePicker(
        // context: context,
        // firstDate: DateTime.now(),
        // lastDate: DateTime.now().add(new Duration(days: 70)),
        // helpText: "إختر تاريخ الحجز",
        // cancelText: "ليس الأن",
        // confirmText: "إحجز",
        // errorFormatText: "أدخل تاريخ صحيح",
        // errorInvalidText: "أدخل تاريخ ضمن المجال",
        // builder: (context, Widget child) {
        //   return Theme(
        //       data: ThemeData.light().copyWith(
        //         primaryColor: Settings.Button_Color,
        //         accentColor: Settings.Button_Color,
        //         colorScheme:
        //             ColorScheme.light(primary: Settings.Button_Color),
        //         buttonTheme: ButtonThemeData(
        //             textTheme: ButtonTextTheme.primary),
        //       ),
        //       child: child);
        // });
        bottomNavigationBar: Container(
          width: MediaQuery.of(context).size.width,
          height: 65,
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 30),
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.5),
              spreadRadius: 5,
              blurRadius: 7,
              offset: Offset(0, 3),
            )
          ], color: Colors.white),
          child: ElevatedButton(
            onPressed: () {
              Navigator.pushNamed(context, "booking");
            },
            style: ButtonStyle(
                backgroundColor: MaterialStateProperty.resolveWith<Color>(
                    (Set<MaterialState> color) {
                  return Settings.Button_Color;
                }),
                shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                    RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(30),
                ))),
            child: Text(
              "إحجز الآن",
              textDirection: TextDirection.rtl,
            ),
          ),
        ),
/////////////////////////////////////////     Main content
        body: StreamBuilder(
          stream: _farmBloc.idStream,
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.hasData) {
              return orientation == Orientation.portrait
                  ? buildDetailedView(snapshot, 0.45, 0.48, portrait)
                  : buildDetailedView(snapshot, 0.5, 0.3, portrait);
            } else
              return Center(child: CircularProgressIndicator());
          },
        ));
  }

  buildDetailedView(AsyncSnapshot snapshot, num containerHeight,
      num heightFactor, bool portrait) {
    PageController carouselController = PageController();
    var streamData = snapshot.data;
    List photos = streamData.photos;
    var proccesedPhotos = photos
        .map((e) => CachedNetworkImage(
              imageUrl: e,
              fit: BoxFit.cover,
              placeholder: (context, url) =>
                  Center(child: CircularProgressIndicator()),
            ))
        .toList();
    return Stack(
      children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height * containerHeight,
          alignment: Alignment.topCenter,
          child: Carousel(
            pageController: carouselController,
            dotSize: 8.0,
            indicatorBgPadding: 4,
            dotBgColor: Colors.transparent,
            dotPosition: DotPosition.bottomCenter,
            dotVerticalPadding: 20,
            autoplayDuration: const Duration(seconds: 4),
            dotSpacing: 15.0,
            dotColor: Settings.Button_Color,
            images: proccesedPhotos,
          ),
        ),
        DraggableScrollableSheet(
          initialChildSize: heightFactor,
          minChildSize: heightFactor,
          maxChildSize: 0.8,
          builder: (context, scrollController) {
            return SingleChildScrollView(
              controller: scrollController,
              child: Container(
                padding:
                    EdgeInsets.only(left: 8, top: 25, right: 12, bottom: 18),
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Container(
                      // color: Colors.blue,
                      child: Text(
                        "A3",
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w400,
                            fontSize: 28),
                      ),
                    ),
                    Container(
                      // color: Colors.red,
                      child: Text(
                        streamData.location,
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 19,
                            fontWeight: FontWeight.w200),
                      ),
                    ),
                    SizedBox(height: 16),
                    Container(
                      height: MediaQuery.of(context).size.height * 0.25,
                      // color: Colors.yellow,
                      child: SingleChildScrollView(
                        child: Text(
                          "هذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعةهذه المزرعة",
                          textDirection: TextDirection.rtl,
                          style: TextStyle(
                              color: Colors.black87,
                              fontSize: 18,
                              fontWeight: FontWeight.w200),
                        ),
                      ),
                    ),
                    SizedBox(height: 16),
                    Container(
                      // color: Colors.green,
                      child: Text(
                        "المميزات",
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 19,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                    portrait
                        ? Container(
                            width: MediaQuery.of(context).size.width,
                            height: 70,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                iconWidget(
                                  streamData.bbq
                                      ? Settings.Button_Color
                                      : Colors.grey,
                                  "مشوى",
                                  TextDirection.ltr,
                                  AppIcons.bbq,
                                ),
                                SizedBox(width: 18),
                                iconWidget(
                                  !streamData.ac
                                      ? Settings.Button_Color
                                      : Colors.grey,
                                  "مكيف",
                                  TextDirection.rtl,
                                  AppIcons.air_conditioner,
                                ),
                                SizedBox(width: 18),
                                iconWidget(
                                  streamData.ac
                                      ? Settings.Button_Color
                                      : Colors.grey,
                                  "تخت",
                                  TextDirection.ltr,
                                  AppIcons.bed,
                                ),
                                SizedBox(width: 18),
                                iconWidget(
                                  streamData.ac
                                      ? Settings.Button_Color
                                      : Colors.grey,
                                  "Wifi",
                                  TextDirection.ltr,
                                  AppIcons.living_room,
                                ),
                                SizedBox(width: 18),
                                iconWidget(
                                    streamData.ac
                                        ? Settings.Button_Color
                                        : Colors.grey,
                                    "Wifi",
                                    TextDirection.ltr,
                                    FontAwesomeIcons.swimmingPool),
                              ],
                            ))
                        : Container(
                            width: MediaQuery.of(context).size.width - 30,
                            height: 70,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                iconWidget(
                                  streamData.bbq
                                      ? Settings.Button_Color
                                      : Colors.grey,
                                  "مشوى",
                                  TextDirection.ltr,
                                  AppIcons.bbq,
                                ),
                                SizedBox(width: 28),
                                iconWidget(
                                  !streamData.ac
                                      ? Settings.Button_Color
                                      : Colors.grey,
                                  "مكيف",
                                  TextDirection.rtl,
                                  AppIcons.air_conditioner,
                                ),
                                SizedBox(width: 28),
                                iconWidget(
                                  streamData.ac
                                      ? Settings.Button_Color
                                      : Colors.grey,
                                  "Wifi",
                                  TextDirection.ltr,
                                  AppIcons.bed,
                                ),
                                SizedBox(width: 28),
                                iconWidget(
                                  streamData.ac
                                      ? Settings.Button_Color
                                      : Colors.grey,
                                  "Wifi",
                                  TextDirection.ltr,
                                  AppIcons.living_room,
                                ),
                                SizedBox(width: 28),
                                iconWidget(
                                  streamData.ac
                                      ? Settings.Button_Color
                                      : Colors.grey,
                                  "Wifi",
                                  TextDirection.ltr,
                                  FontAwesomeIcons.swimmingPool,
                                ),
                              ],
                            ),
                          ),
                    Container(
                      // color: Colors.green,
                      child: Text(
                        "السعر",
                        textDirection: TextDirection.rtl,
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 19,
                            fontWeight: FontWeight.w300),
                      ),
                    ),
                    Row(
                      children: [
                        Container(
                          padding: EdgeInsets.only(left: 11),
                          // color: Colors.green,
                          child: Text(
                            "${streamData.price}",
                            textDirection: TextDirection.rtl,
                            style: TextStyle(
                                color: Settings.Button_Color,
                                fontSize: 19,
                                fontWeight: FontWeight.w300),
                          ),
                        ),
                        Spacer(),
                        Container(
                          // color: Colors.green,
                          child: Text(
                            "سعر الليلة",
                            textDirection: TextDirection.rtl,
                            style: TextStyle(
                                color: Colors.black,
                                fontSize: 15,
                                fontWeight: FontWeight.w300),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            );
          },
        )
      ],
    );
  }

  Widget iconWidget(
      Color color, String text, TextDirection textDirection, IconData icon) {
    return Container(
      width: 50,
      height: 50,
      child: Column(
        children: [
          Expanded(
            child: Icon(
              icon,
              color: color,
            ),
          ),
          Expanded(
            child: Text(
              text,
              textDirection: textDirection,
              style: TextStyle(color: Colors.black54),
            ),
          )
        ],
      ),
    );
  }
}
