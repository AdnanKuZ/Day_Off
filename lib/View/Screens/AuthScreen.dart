import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:testproject/blocs/userBloc.dart';
import 'package:testproject/common/settings.dart';
import 'package:testproject/View/widgets/textformfield_widget.dart';
import 'package:provider/provider.dart';

enum AuthMode { SignUp, Login }

class AuthPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AuthPageState();
}

class _AuthPageState extends State<AuthPage> {
  UserBloc _loginBloc;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  final Map<String, dynamic> _formData = {
    'fullname': null,
    'password': null,
    'phone': null,
    // 'acceptTerms': false
  };
  bool isLoading = false;
  bool state;
  AuthMode _authMode = AuthMode.Login;
  @override
  void initState() {
    // _loginBloc.autoAuthenticate();
    state = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _loginBloc = Provider.of<UserBloc>(context);
    return Scaffold(
      backgroundColor: Settings.BackGround_Color,
      body: ListView(
        children: [
          Center(
            child: Container(
                padding: EdgeInsets.only(
                  top: MediaQuery.of(context).size.height * 0.12,
                ),
                child: Image(
                  fit: BoxFit.contain,
                  image: AssetImage('assets/logo.png'),
                  alignment: Alignment.center,
                  height: MediaQuery.of(context).size.height * 0.15,
                  width: MediaQuery.of(context).size.width * 0.4,
                )),
          ),
          SizedBox(height: MediaQuery.of(context).size.width * 0.30),
          Form(
            key: _formKey,
            child: Column(children: [
              _buildPhoneTextField(context, _formData),
              _authMode == AuthMode.SignUp
                  ? _buildFullNameField(context, _formData)
                  : Container(),
              Stack(children: [
                _buildPasswordTextField(context, _formData, state),
                Padding(
                  padding: const EdgeInsets.only(top: 17, left: 35.0),
                  child: InkWell(
                      child: Icon(
                        Icons.visibility,
                        color: Settings.Button_Color,
                      ),
                      onTap: () {
                        setState(() {
                          state = !state;
                        });
                      }),
                )
              ]),
            ]),
          ),
          Container(
            height: 50.0,
            margin: EdgeInsets.only(
              right: MediaQuery.of(context).size.width * 0.2,
              left: MediaQuery.of(context).size.width * 0.2,
            ),
            child: isLoading
                ? Center(
                    child: CircularProgressIndicator(),
                  )
                : ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        primary: Settings.Button_Color,
                      ),
                      child: Text(
                        _authMode == AuthMode.Login
                            ? 'إنشاء حساب'
                            : 'تسجيل الدخول',
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                      onPressed: () {
                        _submitForm(context);
                      },
                    ),
                  ),
          ),
          Container(
            height: 50.0,
            margin: EdgeInsets.only(
                right: MediaQuery.of(context).size.width * 0.15,
                left: MediaQuery.of(context).size.width * 0.15,
                top: 20.0),
            child: ClipRRect(
              borderRadius: BorderRadius.circular(15),
              child: ElevatedButton(
                child: Text(
                  _authMode == AuthMode.Login ? 'تسجيل الدخول' : 'إنشاء حساب',
                  style: TextStyle(fontSize: 20, color: Settings.Button_Color),
                ),
                onPressed: () {
                  setState(() {
                    _authMode = _authMode == AuthMode.Login
                        ? AuthMode.SignUp
                        : AuthMode.Login;
                  });
                },
                style: ElevatedButton.styleFrom(
                  primary: Colors.white,
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _submitForm(BuildContext context) async {
    if (!_formKey.currentState.validate()) {
      return;
    }
    _formKey.currentState.save();

    if (_authMode == AuthMode.Login) {
      _loginBloc.login(_formData['phone'], _formData['password']);
    } else {
      _loginBloc.signUp(
          _formData['phone'], _formData['fullname'], _formData['password']);
    }
    Stream<Map<String, dynamic>> _snapshot = _loginBloc.userStream;
    setState(() {
      isLoading = true;
    });
    await for (Map<String, dynamic> data in _snapshot) {
      if (data['success']) {
        Navigator.pushNamed(context, 'home');
        setState(() {
          isLoading = false;
        });
        break;
      } else {
        showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text('Auth failed'),
              content: Text(data['message']),
              actions: <Widget>[
                TextButton(
                  child: Text('Okay'),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
        setState(() {
          isLoading = false;
        });
        break;
      }
    }
  }
}

Widget _buildPhoneTextField(BuildContext context, _formData) {
  Widget _phone = Padding(
      padding: EdgeInsets.only(
          left: MediaQuery.of(context).size.width * 0.07,
          right: MediaQuery.of(context).size.width * 0.07),
      child: CustomTextFormField(
        hide: false,
        onSaved: (value) {
          _formData['phone'] = value;
        },
        validator: (value) {
          if (value.isEmpty ||
              !RegExp(r'^(?:[1-9]\d*|0)?(?:\.\d+)?$').hasMatch(value))
            return 'phone number not vaild';
          else
            return null;
        },
        inputType: TextInputType.number,
        hint: 'أدخل رقم هاتفك',
      ));
  return _phone;
}

Widget _buildPasswordTextField(BuildContext context, _formData, state) {
  Widget _password = Padding(
      padding: EdgeInsets.only(
          left: MediaQuery.of(context).size.width * 0.07,
          right: MediaQuery.of(context).size.width * 0.07),
      child: CustomTextFormField(
        hide: state,
        onSaved: (value) => _formData['password'] = value,
        validator: (value) {
          if (value.isEmpty || value.length < 6)
            return 'password invalid';
          else
            return null;
        },
        hint: 'أدخل كلمة السر',
        inputType: TextInputType.text,
      ));

  return _password;
}

Widget _buildFullNameField(BuildContext context, _formData) {
  Widget _buildFullNameTextField = Padding(
      padding: EdgeInsets.only(
          left: MediaQuery.of(context).size.width * 0.07,
          right: MediaQuery.of(context).size.width * 0.07),
      child: CustomTextFormField(
        hide: false,
        onSaved: (value) => _formData['fullname'] = value,
        validator: (value) {
          if (value.isEmpty)
            return 'please type your fullname';
          else
            return null;
        },
        hint: 'أدخل اسمك الكامل',
        inputType: TextInputType.text,
      ));

  return _buildFullNameTextField;
}
