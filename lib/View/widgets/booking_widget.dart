import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_datepicker/datepicker.dart';
import 'package:testproject/common/settings.dart';
import 'package:testproject/blocs/booking_bloc.dart';
import 'package:testproject/repositories/farm_repos.dart';

class BookingCalenderWidget extends StatelessWidget {
  // BookingBloc _bookingBloc;
  final int id;
  final BookingBloc bookingBloc;
  final AsyncSnapshot snapshot;
  BookingCalenderWidget(this.id, this.bookingBloc, this.snapshot);
  @override
  Widget build(BuildContext context) {
    print("$id");
    DateRangePickerController _dateRangePickerController =
        DateRangePickerController();
    return SfDateRangePicker(
      backgroundColor: Colors.white,
      cancelText: "ليس الأن",
      confirmText: "إحجز",
      minDate: DateTime.now(),
      maxDate: DateTime.now().add(new Duration(days: 70)),
      selectionMode: DateRangePickerSelectionMode.multiple,
      selectionColor: Settings.Button_Color,
      view: DateRangePickerView.month,
      controller: _dateRangePickerController,
      showActionButtons: true,
      showNavigationArrow: true,
      monthViewSettings:
          DateRangePickerMonthViewSettings(blackoutDates: snapshot.data),
      monthCellStyle: DateRangePickerMonthCellStyle(
          blackoutDatesDecoration:
              BoxDecoration(color: Colors.red, shape: BoxShape.circle),
          todayCellDecoration: BoxDecoration(
              border: Border.all(
                  color: Settings.Button_Color, style: BorderStyle.solid),
              shape: BoxShape.circle)),
      onCancel: () {
        Navigator.pop(context);
      },
      onSubmit: (Object value) {
        bookingBloc.addBookingDates(value);
        Navigator.pop(context);
      },
    );
  }
}
