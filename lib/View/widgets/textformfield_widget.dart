import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:testproject/common/settings.dart';

// typedef OnSaved = void Function(dynamic value);

class CustomTextFormField extends StatelessWidget {
  final Function onSaved;
  final String hint;
  final FormFieldValidator<String> validator;
  final TextInputType inputType;
  final bool hide;

  CustomTextFormField(
      {@required this.onSaved,
      @required this.validator,
      this.hint,
      this.inputType,
      this.hide});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * 0.13,
      child: TextFormField(
        obscureText: hide,
        textAlign: TextAlign.right,
        onSaved: onSaved,
        validator: validator,
        decoration: InputDecoration(
          contentPadding: EdgeInsets.only(right: 14.5, top: 8, bottom: 8),
          filled: true,
          fillColor: Colors.white,
          border: OutlineInputBorder(
              borderSide: BorderSide(color: Settings.Form_Border_Color),
              borderRadius: BorderRadius.all(Radius.circular(25))),
          hintText: hint,
        ),
        style: TextStyle(color: Colors.grey.shade700, fontSize: 22),
        keyboardType: inputType,
      ),
    );
  }
}
