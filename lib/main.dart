import 'package:flutter/material.dart';
import 'package:testproject/View/Screens/AuthScreen.dart';
import 'package:testproject/View/Screens/DetailedViewPage.dart';
import 'package:testproject/View/Screens/BookingScreen.dart';
import 'package:testproject/View/widgets/booking_widget.dart';
import 'package:testproject/blocs/booking_bloc.dart';
import 'package:testproject/blocs/farm_bloc.dart';
import 'package:testproject/blocs/userBloc.dart';
import 'package:testproject/View/screens/HomePage.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        Provider<UserBloc>(
          create: (context) => UserBloc(),
        ),
        Provider<IdBloc>(
          create: (context) => IdBloc(),
        ),
      ],
      child: MaterialApp(
        home: AuthPage(),
        debugShowCheckedModeBanner: false,
        routes: {
          'home': (context) => HomePage(),
          'auth': (context) => AuthPage(),
          'detailed': (context) => DetailedViewPage(),
          'booking': (context) => BookingScreen(),
        },
      ),
    );
  }
}
