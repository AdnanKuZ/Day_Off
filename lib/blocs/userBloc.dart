import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

// import 'package:shared_preferences/shared_preferences.dart';

class UserBloc {
  StreamController<Map<String, dynamic>> _userStreamController =
      StreamController<Map<String, dynamic>>.broadcast();
  Stream<Map<String, dynamic>> get userStream => _userStreamController.stream;

  Future<void> login(String phone, String password) async {
    // SharedPreferences _prefs = await SharedPreferences.getInstance();
    final Map<String, dynamic> authData = {
      'phone': phone,
      'password': password,
    };
    final http.Response response = await http.post(
        Uri.parse('http://213.239.205.250:8080/users/login'),
        body: json.encode(authData),
        headers: {'Content-Type': 'application/json'});
    bool hasError = true;
    String message = 'Something went wrong.';
    if (response.statusCode == 201) {
      hasError = false;
      message = 'Authentication succeeded!';
      // _prefs.setString('token', response.body);
      // _prefs.setString('phonenumber', phone);
      // _prefs.setString('password', password);
    } else {
      hasError = true;
      message = 'Error';
    }
    Map<String, dynamic> _map = {'success': !hasError, 'message': message};
    _userStreamController.sink.add(_map);
  }

  Future<void> signUp(
    String phone,
    String fullname,
    String password,
  ) async {
    // SharedPreferences _prefs = await SharedPreferences.getInstance();
    final Map<String, dynamic> authData = {
      'fullname': fullname,
      'password': password,
      'phone': phone,
    };
    final http.Response response = await http.post(
      Uri.parse('http://213.239.205.250:8080/users/register'),
      body: json.encode(authData),
      headers: {'Content-Type': 'application/json'},
    );
    bool hasError = true;
    String message = 'Something went wrong.';
    if (response.statusCode == 201) {
      hasError = false;
      message = 'SignUp success';
    } else {
      hasError = true;
      message = 'SignUp failed';
    }

    Map<String, dynamic> _map = {'success': !hasError, 'message': message};
    _userStreamController.sink.add(_map);
  }

  void dispose() {
    _userStreamController.close();
  }

//   void autoAuthenticate() async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();
//     String token = prefs.getString('token');
//     if (token != null) {
//       String phone = prefs.getString('phonenumber');
//       String password = prefs.getString('password');
//       login(phone, password);
//     }
//   }
}
