import 'dart:async';
import 'package:testproject/models/catigory_model.dart';
import 'package:testproject/models/farmId_model.dart';
import 'package:testproject/models/fav_farm_model.dart';
import 'package:testproject/repositories/farm_repos.dart';
import 'package:rxdart/rxdart.dart';

final FarmRepositories farmRepo = FarmRepositories();

class FarmBloc {
  BehaviorSubject<List<FavFarmModel>> favStreamController =
      BehaviorSubject<List<FavFarmModel>>();
  BehaviorSubject<List<CatigoryModel>> catigoryStreamController =
      BehaviorSubject<List<CatigoryModel>>();
  BehaviorSubject<FarmIdModel> idStreamController =
      BehaviorSubject<FarmIdModel>();
  void getFavFarm() async {
    final favFarmData = await farmRepo.getFavFarms();
    favStreamController.sink.add(favFarmData);
  }

  void getCatigoryFarm(String location) async {
    final catigoryFarmData = await farmRepo.getByCatigory(location);
    catigoryStreamController.sink.add(catigoryFarmData);
  }

  void getIdFarm(int id) async {
    final idFarmData = await farmRepo.getById(id);
    idStreamController.sink.add(idFarmData);
  }

  get favStream => favStreamController.stream;
  get catigoryStream => catigoryStreamController.stream;
  get idStream => idStreamController.stream;

  void dispose() {
    favStreamController.close();
    catigoryStreamController.close();
    idStreamController.close();
  }
}

class IdBloc {
  int _id = 0;
  int get id => _id;
  set setId(int id) {
    _id = id;
  }
}
