import 'dart:async';
import 'package:intl/intl.dart';
import 'package:rxdart/rxdart.dart';
import 'package:testproject/repositories/farm_repos.dart';

class BookingBloc {
  FarmRepositories _farmRepositories = FarmRepositories();
  BehaviorSubject<List<DateTime>> bookingDateRangeStreamController =
      BehaviorSubject<List<DateTime>>();

  BehaviorSubject<List<DateTime>> reservedDatesStreamController =
      BehaviorSubject<List<DateTime>>();

  void addBookingDates(List<DateTime> dates) {
    bookingDateRangeStreamController.sink.add(dates);
  }

  void addReservedDates(int id) async {
    List<DateTime> reservations =
        await _farmRepositories.getFarmReservationsById(id);
    reservedDatesStreamController.sink.add(reservations);
  }

  get bookingDatesStream => bookingDateRangeStreamController.stream;
  get reservedDatesStream => reservedDatesStreamController.stream;

  void dispose() {
    bookingDateRangeStreamController.close();
    reservedDatesStreamController.close();
  }
}
