// To parse this JSON data, do
//
//     final farmIdModel = farmIdModelFromJson(jsonString);

import 'dart:convert';

FarmIdModel farmIdModelFromJson(String str) =>
    FarmIdModel.fromJson(json.decode(str));

String farmIdModelToJson(FarmIdModel data) => json.encode(data.toJson());

class FarmIdModel {
  FarmIdModel({
    this.id,
    this.location,
    this.price,
    this.lat,
    this.land,
    this.info,
    this.ac,
    this.rooms,
    this.bbq,
    this.pool,
    this.photos,
  });

  int id;
  String location;
  int price;
  String lat;
  String land;
  String info;
  bool ac;
  int rooms;
  bool bbq;
  int pool;
  List<String> photos;

  factory FarmIdModel.fromJson(Map<String, dynamic> json) => FarmIdModel(
        id: json["id"],
        location: json["location"],
        price: json["price"],
        lat: json["lat"],
        land: json["land"],
        info: json["info"],
        ac: json["AC"],
        rooms: json["rooms"],
        bbq: json["bbq"],
        pool: json["pool"],
        photos: List<String>.from(json["photos"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "location": location,
        "price": price,
        "lat": lat,
        "land": land,
        "info": info,
        "AC": ac,
        "rooms": rooms,
        "bbq": bbq,
        "pool": pool,
        "photos": List<dynamic>.from(photos.map((x) => x)),
      };
}
