// To parse this JSON data, do
//
//     final catigoryModel = catigoryModelFromJson(jsonString);

import 'dart:convert';

List<CatigoryModel> catigoryModelFromJson(String str) =>
    List<CatigoryModel>.from(
        json.decode(str).map((x) => CatigoryModel.fromJson(x)));

String catigoryModelToJson(List<CatigoryModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class CatigoryModel {
  CatigoryModel({
    this.id,
    this.location,
    this.price,
    this.photos,
  });

  int id;
  String location;
  int price;
  List<String> photos;

  factory CatigoryModel.fromJson(Map<String, dynamic> json) => CatigoryModel(
        id: json["id"],
        location: json["location"],
        price: json["price"],
        photos: List<String>.from(json["photos"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "location": location,
        "price": price,
        "photos": List<dynamic>.from(photos.map((x) => x)),
      };
}
