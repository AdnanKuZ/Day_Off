// To parse this JSON data, do
//
//     final favFarmModel = favFarmModelFromJson(jsonString);

import 'dart:convert';

List<FavFarmModel> favFarmModelFromJson(String str) => List<FavFarmModel>.from(
    json.decode(str).map((x) => FavFarmModel.fromJson(x)));

String favFarmModelToJson(List<FavFarmModel> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class FavFarmModel {
  FavFarmModel({
    this.id,
    this.location,
    this.price,
    this.photos,
  });

  int id;
  String location;
  int price;
  List<String> photos;

  factory FavFarmModel.fromJson(Map<String, dynamic> json) => FavFarmModel(
        id: json["id"],
        location: json["location"],
        price: json["price"],
        photos: List<String>.from(json["photos"].map((x) => x)),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "location": location,
        "price": price,
        "photos": List<dynamic>.from(photos.map((x) => x)),
      };
}
